package com.apexsupplychain;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apexsupplychain.clickandcollect.Action;
import com.apexsupplychain.clickandcollect.SysManagerResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


public class SysManagerJSONTests {
  
 private static final Logger logger = LoggerFactory.getLogger(SysManagerJSONTests.class);
  

 @Test
 public void testJsonMapper() throws JsonProcessingException {
 	ObjectMapper mapper = new ObjectMapper();
 	 
 	Action action = new Action("systemManager.str1.module1.display1",
 	"display(Please go to locker 2 to take your order.)",
 	true,
 	true);
 	List<String> nextMessages = new ArrayList<String>();
 	nextMessages.add("NextActionReq");
 	List<Action> actions = new ArrayList<Action>();
 	actions.add(action);
 	actions.add(action);
 	 
 	SysManagerResponse sysManagerResponse = new SysManagerResponse("StartTxnRes", "OK", "93DFGs72ne03", nextMessages, actions);
 	 
//String sysManagerResponseJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(sysManagerResponse);
com.fasterxml.jackson.databind.JsonNode rootNode = mapper.convertValue(sysManagerResponse, com.fasterxml.jackson.databind.JsonNode.class);
JsonNode actionNode = rootNode.path("actions");
for (JsonNode actionArray : actionNode) {
	    if (actionArray instanceof ObjectNode) {
	        ObjectNode object = (ObjectNode) actionArray;
	        object.remove("stopIfFail");
	    }
	}


System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode));
//logger.info("Json String: {}", sysManagerResponseJson);
	//assertTrue(sysManagerResponseJson.contains("StartTxnRes"));
}


 	 
 	 
}
