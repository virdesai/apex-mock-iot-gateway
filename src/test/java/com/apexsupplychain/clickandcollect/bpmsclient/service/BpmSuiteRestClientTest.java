package com.apexsupplychain.clickandcollect.bpmsclient.service;

import static org.junit.Assert.assertNotNull;


import java.net.MalformedURLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.services.client.serialization.jaxb.impl.process.JaxbProcessInstanceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import com.apexsupplychain.clickandcollect.SysManagerStart;
import com.apexsupplychain.clickandcollect.bpmsclient.config.MainConfig;
import com.apexsupplychain.clickandcollect.bpmsclient.service.BpmSuiteRestClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=MainConfig.class)
public class BpmSuiteRestClientTest {
	
	@Autowired
	private BpmSuiteRestClient bpmSuiteRestClient;

	@Test
	public void testPickUpWorkFlow() {
		MultiValueMap<String, String> queryParameters = buildQueryParamsForCashAndCarry("pickUpCode", "1578TN", "Bay5");
		JaxbProcessInstanceResponse result = bpmSuiteRestClient.startWorkFlow(queryParameters, "ClickandCollect.PickUp");
		assertNotNull(result);
	}
	
	
	@Test
	public void testExpeditorDeliveryWorkflow() throws JsonProcessingException {
		SysManagerStart sysManagerStart = new SysManagerStart("Px9dTC3608", "000AC263690D", "300294756209", "systemManager.str1.module1.reader");
		MultiValueMap<String, String> queryParameters = buildQueryParamsForCashAndCarry(sysManagerStart);
		JaxbProcessInstanceResponse result = bpmSuiteRestClient.startWorkFlow(queryParameters, "ClickandCollect.ExpeditorDelivery");

		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result.getId()), "bump");
	
		assertNotNull(result);
	}
	
	@Test
	public void testExpeditorDeliveryWorkflowUsingSysManagerStartWithBumpPlusOne() throws JsonProcessingException {
		SysManagerStart sysManagerStart = new SysManagerStart("Px9dTC3608", "000AC263690D", "300294756209", "systemManager.str1.module1.reader");
		MultiValueMap<String, String> queryParameters = buildQueryParamsForCashAndCarry(sysManagerStart);
		JaxbProcessInstanceResponse result = bpmSuiteRestClient.startWorkFlow(queryParameters, "ClickandCollect.ExpeditorDelivery");

		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result.getId()), "plusone");
		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result.getId()), "plusone");

		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result.getId()), "bump");
		
		assertNotNull(result);
	}
	
	
	@Test
	public void testExpeditorDeliveryWorkflowForTwoOrders() throws JsonProcessingException {
		SysManagerStart sysManagerStart = new SysManagerStart("Px9dTC3608", "000AC263690D", "300294756209", "systemManager.str1.module1.reader");
		SysManagerStart sysManagerStart2 = new SysManagerStart("Px9dTC3608", "000AC263690D", "30029473333", "systemManager.str1.module2.reader");

		MultiValueMap<String, String> queryParameters = buildQueryParamsForCashAndCarry(sysManagerStart);
		MultiValueMap<String, String> queryParameters2 = buildQueryParamsForCashAndCarry(sysManagerStart2);
		JaxbProcessInstanceResponse result = bpmSuiteRestClient.startWorkFlow(queryParameters, "ClickandCollect.ExpeditorDelivery");
		JaxbProcessInstanceResponse result2 = bpmSuiteRestClient.startWorkFlow(queryParameters2, "ClickandCollect.ExpeditorDelivery");

		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result.getId()), "plusone");
		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result2.getId()), "plusone");
		
		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result.getId()), "plusone");
		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result2.getId()), "plusone");
		
		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result.getId()), "bump");
		bpmSuiteRestClient.signalWorkFlow(String.valueOf(result2.getId()), "bump");
		

		assertNotNull(result);
		assertNotNull(result);
	}

	
	private MultiValueMap<String, String> buildQueryParamsForCashAndCarry(SysManagerStart sysManagerStart) throws JsonProcessingException {
		
		String sysManagerStartJSON = new ObjectMapper().writeValueAsString(sysManagerStart); 
		
		MultiValueMap<String, String> params =  new LinkedMultiValueMap<String, String>();
		params.set("map_sysmanagerstartInput", sysManagerStartJSON);
		return params;
	}
	
	private MultiValueMap<String, String> buildQueryParamsForCashAndCarry(String cashAndCarryAppCode, String sysManagerSN, String inputSourceBay) {
		MultiValueMap<String, String> params =  new LinkedMultiValueMap<String, String>();
		params.set("map_cashAndCarryAppCode", cashAndCarryAppCode);
		params.set("map_sysManagerSN", sysManagerSN);
		params.set("map_inputBay", inputSourceBay);
		
		return params;
	}

}
