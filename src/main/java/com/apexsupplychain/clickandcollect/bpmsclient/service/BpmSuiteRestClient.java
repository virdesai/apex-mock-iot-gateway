package com.apexsupplychain.clickandcollect.bpmsclient.service;

import java.net.MalformedURLException;
import java.net.URL;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;
import org.kie.services.client.serialization.jaxb.impl.process.JaxbProcessInstanceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;



@Service
public class BpmSuiteRestClient {
	
	private static final Logger log = LoggerFactory.getLogger(BpmSuiteRestClient.class);
	private static final String BASE_URL = "http://localhost:8080/business-central/rest/";
	private static final String DEPLOYMENT_ID = "com.apexsupplychain:clickandcollect:1.0.0-SNAPSHOT";
	private static final String BASE_URL2 = "http://localhost:8080/business-central/";
	private final RestTemplate restTemplate;
	
	public BpmSuiteRestClient(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	private HttpHeaders buildAuthHeader() {
		String plainCreds = "bpmsAdmin:bpmsuite1!";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64Utils.encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		
		return headers;
	}
	
	public JaxbProcessInstanceResponse startWorkFlow(MultiValueMap<String, String> queryParameters, String processDefId) {
		
		String url = BASE_URL + "runtime/" + DEPLOYMENT_ID + "/process/" + processDefId + "/start";
	 	HttpEntity<String> request = new HttpEntity<String>(buildAuthHeader());
	 	UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url).queryParams(queryParameters);
	 	
	 	log.debug(builder.build().toUriString());
	 	//ResponseEntity<String> response = restTemplate.exchange(builder.build().toUriString(), HttpMethod.POST, request, String.class);

	 	ResponseEntity<JaxbProcessInstanceResponse> jaxbProcessInstanceResponse = restTemplate.exchange(builder.build().toUri(), HttpMethod.POST, request, JaxbProcessInstanceResponse.class);
	 	
	 	
	 	log.debug("result of POST: {}", jaxbProcessInstanceResponse.getBody());
	
	 	return jaxbProcessInstanceResponse.getBody();

	}
	


	public String signalWorkFlow(String id, String signal) {
		String url = BASE_URL + "runtime/" + DEPLOYMENT_ID + "/process/instance/" + id + "/signal?signal={signal}";
	
		HttpEntity<String> request = new HttpEntity<String>(buildAuthHeader());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class, signal);
		
		log.debug("result of POST: {}", response.getBody());
		return response.getBody();
		
	}
	
	
	public void signalWorkFlow2(String id, String signal) throws MalformedURLException {
		
		Long id2 = Long.parseLong(id);
		
		RuntimeEngine engine = RemoteRuntimeEngineFactory.newRestBuilder()
			     .addDeploymentId(DEPLOYMENT_ID)
			     .addUrl(new URL(BASE_URL2))
			     .addUserName("bpmsAdmin")
			     .addPassword("bpmsuite1!")
			     .build();
		
		KieSession ksession = engine.getKieSession();
		
		ksession.signalEvent(signal, String.class, id2.longValue());
	   
		
	}

}
