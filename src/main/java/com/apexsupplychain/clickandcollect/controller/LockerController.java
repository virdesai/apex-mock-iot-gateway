package com.apexsupplychain.clickandcollect.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apexsupplychain.clickandcollect.ActionResult;
import com.apexsupplychain.clickandcollect.SysManagerRequest;
import com.apexsupplychain.clickandcollect.SysManagerResponse;
import com.apexsupplychain.clickandcollect.model.LockerLocation;
import com.apexsupplychain.clickandcollect.model.Position;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/api/locker")
public class LockerController {
	
	private static final Logger logger = LoggerFactory.getLogger(LockerController.class);
	
	@RequestMapping(method=RequestMethod.POST, value="/unlockedcolor")
	public String unlockedColor(@RequestParam("input") String input){
			logger.debug("inputs: {}", input);
		
		return "blue";
		}
	
	@RequestMapping(method=RequestMethod.POST, value="/lockedcolor")
	public String lockedColor(@RequestParam("input") String input){
			logger.debug("inputs: {}", input);
		
		return "red";
		}
	
	@RequestMapping(method=RequestMethod.POST, value="/lockerlocations")
	public LockerLocation lockerlocations(@RequestParam("input") String input){
			logger.debug("inputs: {}", input);
			Position position1 = new Position("01");
			Position position2 = new Position("23");
			Position position3 = new Position("04");
			LockerLocation lockerLocation = new LockerLocation(new ArrayList<Position>());
			lockerLocation.getPositions().add(position1);
			lockerLocation.getPositions().add(position2);
			lockerLocation.getPositions().add(position3);
			return lockerLocation;
	
		
		}
	
	@RequestMapping(method=RequestMethod.POST, value="/openlocker")
	public SysManagerRequest openLocker(@RequestBody String sysManagerResponseJson) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		SysManagerResponse sysManagerResponse = mapper.readValue(sysManagerResponseJson, SysManagerResponse.class);
		ActionResult actionResultLock = new ActionResult(sysManagerResponse.getActions().get(2).getResource(),
				"doorStatus(\"state\")",
				new String[] {"ng"},
				"OK");
		
		SysManagerRequest sysManagerRequest = new SysManagerRequest(new ArrayList<ActionResult>());
		sysManagerRequest.getActionResults().add(actionResultLock);
		
		return sysManagerRequest;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/delivery/loadlockercolor")
	public String deliveryLockerColor(@RequestParam("userInput") String userInput, 
								@RequestParam("systemMgrSN") String systemMgrSN){		
		logger.debug("userInput: {}", userInput+" "+systemMgrSN);		
		return "red";
		}
	
	@RequestMapping(method=RequestMethod.GET, value="/delivery/firstlocker")
	public String lockerForDelivery(@RequestParam("userInput") String userInput, 
			@RequestParam("systemMgrSN") String systemMgrSN) {
		Random rand = new Random();
		return String.valueOf(rand.nextInt(20) + 1);	
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/delivery/loadnextlocker")
	public SysManagerRequest deliveryLoadNextLocker(@RequestBody String sysManagerResponseJson) throws JsonParseException, JsonMappingException, IOException{		
		ObjectMapper mapper = new ObjectMapper();
		
		SysManagerResponse sysManagerResponse = mapper.readValue(sysManagerResponseJson, SysManagerResponse.class);
		
		ActionResult actionDisplay = new ActionResult(sysManagerResponse.getActions().get(0).getResource(),
				"display(content)",
				new String[] {sysManagerResponse.getActions().get(0).getFunction()},
				"OK");
		
		ActionResult actionLight = new ActionResult(sysManagerResponse.getActions().get(1).getResource(),
				"light(state, color, intensity)",
				new String[] {sysManagerResponse.getActions().get(1).getFunction()},
				"OK");
		
		ActionResult actionSensor = new ActionResult(sysManagerResponse.getActions().get(2).getResource(),
				"weightSensor(state)",
				new String[] {"present"},
				"OK");
		
		SysManagerRequest sysManagerRequest = new SysManagerRequest(new ArrayList<ActionResult>());
		sysManagerRequest.getActionResults().add(actionDisplay);
		sysManagerRequest.getActionResults().add(actionLight);
		sysManagerRequest.getActionResults().add(actionSensor);
		
		return sysManagerRequest;
		}
	
}
