package com.levvel.sample.model;


public class Locker implements java.io.Serializable
{

   static final long serialVersionUID = 1L;

   private java.lang.String position;

   public Locker()
   {
   }

   public java.lang.String getPosition()
   {
      return this.position;
   }

   public void setPosition(java.lang.String position)
   {
      this.position = position;
   }

   public Locker(java.lang.String position)
   {
      this.position = position;
   }

}
