package com.levvel.sample.controller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.levvel.sample.model.Locker;
import com.levvel.sample.model.LockerLocation;

@RestController
@RequestMapping("/api/locker")
public class LockerController {
	
	private static final Logger logger = LoggerFactory.getLogger(LockerController.class);
	
	@RequestMapping(method=RequestMethod.POST, value="/changecolor", produces="application/json")
	public LockerLocation changeLockerColor(@RequestParam("input") String input){
		LockerLocation lockerLocation = new LockerLocation("walmart-250",new ArrayList<Locker>());
			logger.debug("inputs: {}", input);
		
		
		Locker locker1 = new Locker("1");
		Locker locker2 = new Locker("2");
		lockerLocation.getLockers().add(locker1);
		lockerLocation.getLockers().add(locker2);
		
		return lockerLocation;
	}

}


